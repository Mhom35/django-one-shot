python -m venv .venv
source .venv/bin/activate
pip install django

pip install black
pip install flake8
pip install djlint
Deactivate your virtual environment
Activate your virtual environment
git add .
git commit -m ""
git push

django-admin startproject "brain_two" .

python manage.py startapp "todos"
python manage.py migrate
python manage.py createsuperuser
