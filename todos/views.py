from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.


class ToDoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class ToDoItemDetail(DetailView):
    model = TodoList
    template_name = "todos/details.html"


class ToDoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

    # def form_valid(self, form):
    #     # Save the todolist, but don't put it in the database
    #     todolist = form.save(commit=False)
    #     # Assign the owner to the todolist
    #     todolist.owner = self.request.user

    #     # Now, save it to the database
    #     todolist.save()
    #     # Save all of the many-to-many relationships
    #     form.save_o2m()

    #     # Redirect to the detail page for the meal plan
    #     return redirect("meal_plan_detail", pk=todolist.id)


class ToDoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/update.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class ToDoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class ToDoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/item_create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class ToDoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/item_update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
