from django.urls import path

from todos.views import (
    ToDoListView,
    ToDoItemDetail,
    ToDoListCreateView,
    ToDoListUpdateView,
    ToDoListDeleteView,
    ToDoItemCreateView,
    ToDoItemUpdateView,
)

urlpatterns = [
    path("", ToDoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", ToDoItemDetail.as_view(), name="todo_list_detail"),
    path("create/", ToDoListCreateView.as_view(), name="todo_list_create"),
    path(
        "<int:pk>/edit/", ToDoListUpdateView.as_view(), name="todo_list_update"
    ),
    path(
        "<int:pk>/delete/",
        ToDoListDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path(
        "items/create/", ToDoItemCreateView.as_view(), name="todo_item_create"
    ),
    path(
        "items/<int:pk>/edit/",
        ToDoItemUpdateView.as_view(),
        name="todo_item_update",
    ),
]
